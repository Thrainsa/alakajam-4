﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alakajam
{
    public class ArmyOrderController : MonoBehaviour
    {

        public enum State
        {
            NO_ORDER,
            AIM_FOR_TARGET
        }

        private const float selectDistance = 20;
        private int layerMaskOrder;
        private int layerMaskTarget;

        public GameObject targetMarkerPrefab;
        private PlayerInput playerInput;
        private PlayerController playerController;
            
        [Header("Runtime state")]

        public State orderState;
        
        public ArmyAI hoveredFriendlyArmyAI;
        public ArmyAI selectedArmyAI;
        public ArmyAI hoveredTargetArmyAI;
        public GameObject targetMarkerInstance;

        private void Start()
        {
             layerMaskOrder = LayerMask.GetMask(new string[] { "Enemy" });
             layerMaskTarget = LayerMask.GetMask(new string[] { "Enemy", "Default" });
             playerController = PlayerController.instance;
             playerInput = PlayerInput.Instance;
        }

        void Update()
        {

            if (orderState == State.NO_ORDER)
            {
                // Look for friendly army
                RefreshHovered();

                // Detect friendly army selection
                if (playerInput.Interact && hoveredFriendlyArmyAI != null)
                {
                    OnArmySelect();
                }
            }
            else
            {
                // Look for any target
                RefreshTargetMarker();

                if (playerInput.Interact)
                {
                    // Set destination
                    OnTargetSelect();
                }
                else if (playerInput.JumpInput)
                {
                    // Cancel
                    OnArmyCancel();
                }
            }

            playerInput.SetAttackBlocked(orderState == State.AIM_FOR_TARGET || hoveredFriendlyArmyAI != null);
        }

        void RefreshHovered()
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hitInfo = new RaycastHit();
            Physics.Raycast(ray, out hitInfo, selectDistance, layerMaskOrder);

            Debug.DrawLine(transform.position, transform.position + transform.forward * selectDistance, Color.red);

            bool hover = false;
            if (hitInfo.collider != null)
            {
                ArmyAI armyAi = hitInfo.collider.GetComponent<ArmyAI>();
                if (armyAi != null && armyAi.isPlayerArmy)
                {
                    SetHoveredFriendlyArmyAI(armyAi);
                    armyAi.SetHoverVisible(true);
                    hover = true;
                }
            }
            
            if (!hover)
            {
                SetHoveredFriendlyArmyAI(null);
            }

        }

        void OnArmySelect()
        {
            selectedArmyAI = hoveredFriendlyArmyAI;
            selectedArmyAI.SetHoverVisible(false);
            orderState = State.AIM_FOR_TARGET;
        }

        void RefreshTargetMarker()
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hitInfo = new RaycastHit();
            Physics.Raycast(ray, out hitInfo, 200f, layerMaskTarget);

            Debug.DrawRay(transform.position, transform.forward, Color.yellow);

            bool hover = false;
            if (hitInfo.collider != null)
            {
                ArmyAI armyAi = hitInfo.collider.GetComponent<ArmyAI>();
                if (armyAi != null && armyAi != selectedArmyAI)
                {
                    // Mark existing target
                    SetHoveredTargetArmyAI(armyAi);
                    hover = true;
                }
                else if (hitInfo.collider.CompareTag("Table"))
                {
                    // Use marker
                    SetHoveredTargetArmyAI(null);
                    targetMarkerInstance.SetActive(true);
                    targetMarkerInstance.transform.position = hitInfo.point + new Vector3(0, 0.3f, 0);
                    hover = true;
                }
            }

            if (!hover)
            {
                SetHoveredTargetArmyAI(null);
                targetMarkerInstance.SetActive(false);
            }
        }

        void SetHoveredFriendlyArmyAI(ArmyAI armyAi)
        {
            if (hoveredFriendlyArmyAI != null)
            {
                hoveredFriendlyArmyAI.SetHoverVisible(false);
            }
            if (armyAi != null)
            {
                armyAi.SetHoverVisible(true);
            }
            hoveredFriendlyArmyAI = armyAi;
        }

        void SetHoveredTargetArmyAI(ArmyAI armyAi, bool forceDestroyTargetMarker = false)
        {
            if (hoveredTargetArmyAI != null)
            {
                hoveredTargetArmyAI.SetHoverVisible(false);
            }

            if (armyAi != null || forceDestroyTargetMarker)
            {
                Destroy(targetMarkerInstance);
                targetMarkerInstance = null;
            }

            if (armyAi != null)
            {
                armyAi.SetHoverVisible(true);
            }
            else if (targetMarkerInstance == null && !forceDestroyTargetMarker)
            {
                targetMarkerInstance = Instantiate(targetMarkerPrefab);
            }

            hoveredTargetArmyAI = armyAi;
        }

        void OnArmyCancel()
        {
            selectedArmyAI = null;
            orderState = State.NO_ORDER;
            SetHoveredTargetArmyAI(null, true);
            SetHoveredFriendlyArmyAI(null);
        }

        void OnTargetSelect()
        {
            if (targetMarkerInstance == null && hoveredTargetArmyAI == null)
            {
                Debug.LogError("No target destination selected!");
            } else if (selectedArmyAI == null)
            {
                Debug.LogError("No army to give orders to!");
            }

            Transform target = hoveredTargetArmyAI ? hoveredTargetArmyAI.transform : targetMarkerInstance.transform;
            bool success = selectedArmyAI.SetDestination(target);
            
            if (!success)
            {
                SetHoveredTargetArmyAI(null, true);
            } else if (targetMarkerInstance)
            {
                // Detach marker
                targetMarkerInstance.transform.localScale *= 0.5f;
                targetMarkerInstance = null;
            }

            OnArmyCancel();
        }
    }


}