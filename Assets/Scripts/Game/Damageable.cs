using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alakajam {
    public class Damageable : MonoBehaviour
    {
        public int maxHealth;
        private IntVariable health;
        public int currentHealth { get { return health.Value; } }
        
        public delegate void Death();
        public event Death OnDeath;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
 
        }

        public bool TakeDamage(int damage){
            health.ApplyChange(-damage);
            if(health.Value <= 0){
                health.SetValue(0);
                OnDeath();
                return true;
            }
            return false;
        }

        public int GainHealth(int amount){
            int healthGained = amount;
            health.ApplyChange(amount);
            if(health.Value > maxHealth){
                healthGained -= health.Value - maxHealth;
                health.SetValue(maxHealth);
            }
            return healthGained;
        }

        public void Init(int value)
        {
            health = ScriptableObject.CreateInstance<IntVariable>();
            health.SetValue(value);
        }
    }
}