using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.AI;

namespace Alakajam
{
    // Mostly from 3DGameKit
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        protected static PlayerController s_Instance;
        public static PlayerController instance { get { return s_Instance; } }

        public bool respawning { get { return m_Respawning; } }
        public bool isOnOlympus { get { return m_isOnOlympus; } }

        public float maxForwardSpeed = 8f;        // How fast Player can run.
        public float gravity = 20f;               // How fast Player accelerates downwards when airborne.
        public float jumpSpeed = 10f;             // How fast Player takes off when jumping.
        public float minTurnSpeed = 400f;         // How fast Player turns when moving at maximum speed.
        public float maxTurnSpeed = 1200f;        // How fast Player turns when stationary.
        public bool canAttack;                    // Whether or not Player can attack.
        public float attackDuration;

        public GameObject jumpCursor;
        public Transform olympusRespawn;
        public CameraSettings cameraSettings;            // Reference used to determine the camera's direction.
        public FallAttack fallAttack;
        public GameObject particuleSystemPrefab;

        protected bool m_IsGrounded = true;            // Whether or not Player is currently standing on the ground.
        protected bool m_PreviouslyGrounded = true;    // Whether or not Player was standing on the ground last frame.
        protected bool m_ReadyToJump;                  // Whether or not the input state and Player are correct to allow jumping.
        protected float m_DesiredForwardSpeed;         // How fast Player aims be going along the ground based on input.
        protected float m_ForwardSpeed;                // How fast Player is currently going along the ground.
        protected float m_VerticalSpeed;               // How fast Player is currently moving up or down.
        protected bool m_respawn;

        protected float m_JumpLoadingTime = 0f;
        protected bool m_isOnOlympus = true;
        protected Vector3 m_fallingDestination = Vector3.zero;
        protected float m_startAttackTime = 0f;
        
        protected PlayerInput m_Input;                 // Reference used to determine how Player should move.
        protected CharacterController m_CharCtrl;      // Reference used to actually move Player.
        protected Animator m_Animator;                 // Reference used to make decisions based on Player's current animation and to set parameters.
        protected Quaternion m_TargetRotation;         // What rotation Player is aiming to have based on input.
        protected float m_AngleDiff;                   // Angle in degrees between Player's current rotation and her target rotation.
        // protected Collider[] m_OverlapResult = new Collider[8];    // Used to cache colliders that are near Player.
        protected bool m_InAttack;                     // Whether Player is currently in the middle of a melee attack.
        // protected bool m_InCombo;                      // Whether Player is currently in the middle of her melee combo.
        
        // protected Damageable m_Damageable;             // Reference used to set invulnerablity and health based on respawning.
        protected Vector3 m_CurrentCheckpoint;      // Reference used to reset Player to the correct position on respawn.
        protected bool m_Respawning;                   // Whether Player is currently respawning.
        protected HashSet<ArmyAI> enemiesInRange = new HashSet<ArmyAI>();

        // These constants are used to ensure Player moves and behaves properly.
        // It is advised you don't change them without fully understanding what they do in code.
        const float k_AirborneTurnSpeedProportion = 5.4f;
        const float k_GroundedRayDistance = 1f;
        const float k_JumpAbortSpeed = 10f;
        const float k_MinEnemyDotCoeff = 0.2f;
        const float k_InverseOneEighty = 1f / 180f;
        const float k_StickingGravityProportion = 0.3f;
        const float k_GroundAcceleration = 20f;
        const float k_GroundDeceleration = 25f;

        protected bool IsMoveInput
        {
            get { return !Mathf.Approximately(m_Input.MoveInput.sqrMagnitude, 0f); }
        }

        public void SetCanAttack(bool canAttack)
        {
            this.canAttack = canAttack;
        }

        // Called automatically by Unity when the script is first added to a gameobject or is reset from the context menu.
        void Reset()
        {
            // meleeWeapon = GetComponentInChildren<MeleeWeapon>();

            cameraSettings = FindObjectOfType<CameraSettings>();

            if (cameraSettings != null)
            {
                if (cameraSettings.follow == null)
                    cameraSettings.follow = transform;

                if (cameraSettings.lookAt == null)
                    cameraSettings.follow = transform.Find("HeadTarget");
            }
        }

        // Called automatically by Unity when the script first exists in the scene.
        void Awake()
        {
            m_Input = GetComponent<PlayerInput>();
            m_Animator = GetComponentInChildren<Animator>();
            m_CharCtrl = GetComponent<CharacterController>();
            s_Instance = this;
            olympusRespawn.transform.parent = null;
        }

        // Called automatically by Unity after Awake whenever the script is enabled. 
        void OnEnable()
        {
            // m_Damageable = GetComponent<Damageable>();
            // m_Damageable.onDamageMessageReceivers.Add(this);

            // m_Damageable.isInvulnerable = true;

            // EquipMeleeWeapon(false);

            // m_Renderers = GetComponentsInChildren<Renderer>();
        }

        // Called automatically by Unity whenever the script is disabled.
        void OnDisable()
        {
            // m_Damageable.onDamageMessageReceivers.Remove(this);
        }

        // Called automatically by Unity once every Physics step.
        void FixedUpdate()
        {
            if(m_fallingDestination != Vector3.zero){
                // The step size is equal to speed times frame time.
                float step = gravity * Time.deltaTime * 2;

                // Move our position a step closer to the target.
                transform.position = Vector3.MoveTowards(transform.position, m_fallingDestination, step);
                if(transform.position == m_fallingDestination){
                    m_fallingDestination = Vector3.zero;
                    m_ForwardSpeed = 0;
                    m_VerticalSpeed = 0;
                    cameraSettings.EndJump();
                }
                return;
            }
            if(m_respawn) {
                m_respawn = false;
                transform.position = olympusRespawn.position;
                transform.eulerAngles = new Vector3(0,180f,0);
                m_VerticalSpeed = 0;
                m_ForwardSpeed = 0;
                return;
            }
            if (m_Input.Attack && canAttack){
                if(m_startAttackTime < Time.time - attackDuration){
                    m_startAttackTime = Time.time;
                    StartAttack();
                }
            }

            CalculateForwardMovement();
            CalculateVerticalMovement();

            SetTargetRotation();

            if (IsMoveInput)
                UpdateOrientation();

            m_PreviouslyGrounded = m_IsGrounded;

            OnAnimatorMove();
            transform.position = GetComponent<NavMeshAgent>().nextPosition;

            if (transform.position.y < -10 && m_VerticalSpeed < 0)
            {
                StartCoroutine(ExecuteJump());
            }
        }

        private void StartAttack()
        {
            m_Animator.SetTrigger("attack");

            if(enemiesInRange.Count > 0){
                List<ArmyAI> itemsToRemove = new List<ArmyAI>();
                bool attackDone = false;
                foreach (var enemy in enemiesInRange)
                {
                    if(enemy.isPlayerArmy){
                        itemsToRemove.Add(enemy);
                    } else {
                        bool dead = enemy.health.currentHealth != 0 ? enemy.TakeDamage(1) : true;
                        attackDone = true;
                        if(dead){
                            itemsToRemove.Add(enemy);
                        }
                    }
                }
                if(attackDone){
                    StartCoroutine(ShowParticules());
                }

                foreach (var item in itemsToRemove)
                {
                    enemiesInRange.Remove(item);
                }
            }
        }

        IEnumerator ShowParticules()
        {
            yield return new WaitForSeconds(0.2f);
            GameObject particules = Instantiate(particuleSystemPrefab, transform.position,Quaternion.identity,transform);
            particules.transform.localPosition = new Vector3(0.46f,0,1.2f);
            yield return new WaitForSeconds(2f);
            Destroy(particules);
        }

        // Called each physics step.
        void CalculateForwardMovement()
        {
            // Cache the move input and cap it's magnitude at 1.
            Vector2 moveInput = m_Input.MoveInput;
            if (moveInput.sqrMagnitude > 1f)
                moveInput.Normalize();

            // Calculate the speed intended by input.
            m_DesiredForwardSpeed = moveInput.magnitude * maxForwardSpeed;

            // Determine change to speed based on whether there is currently any move input.
            float acceleration = IsMoveInput ? k_GroundAcceleration : k_GroundDeceleration;

            // Adjust the forward speed towards the desired speed.
            m_ForwardSpeed = Mathf.MoveTowards(m_ForwardSpeed, m_DesiredForwardSpeed, acceleration * Time.deltaTime);

            // Set the animator parameter to control what animation is being played.
            m_Animator.SetFloat("speed", m_ForwardSpeed);
        }

        // Called each physics step.
        void CalculateVerticalMovement()
        {
            if (!m_Input.JumpInput && m_IsGrounded && m_JumpLoadingTime == 0)
                m_ReadyToJump = true;
            /* ------------- THIS IS FOR NORMAL JUMP, DEACTIVATED FOR NOW -------------- */
            /* 
            // If jump is not currently held and Player is on the ground then she is ready to jump.
            if (m_IsGrounded)
            {
                // When grounded we apply a slight negative vertical speed to make Player "stick" to the ground.
                m_VerticalSpeed = -gravity * k_StickingGravityProportion;

                // If jump is held, Player is ready to jump and not currently in the middle of a melee combo...
                if (m_Input.JumpInput && m_ReadyToJump)
                {
                    // ... then override the previously set vertical speed and make sure she cannot jump again.
                    m_VerticalSpeed = jumpSpeed;
                    m_IsGrounded = false;
                    m_ReadyToJump = false;
                }
            }
            else
            {
                // If Player is airborne, the jump button is not held and Player is currently moving upwards...
                if (!m_Input.JumpInput && m_VerticalSpeed > 0.0f)
                {
                    // ... decrease Player's vertical speed.
                    // This is what causes holding jump to jump higher that tapping jump.
                    m_VerticalSpeed -= k_JumpAbortSpeed * Time.deltaTime;
                }

                // If a jump is approximately peaking, make it absolute.
                if (Mathf.Approximately(m_VerticalSpeed, 0f))
                {
                    m_VerticalSpeed = 0f;
                }
                
                // If Player is airborne, apply gravity.
                m_VerticalSpeed -= gravity * Time.deltaTime;
            }
             */

            if (m_IsGrounded)
            {
                // When grounded we apply a slight negative vertical speed to make Player "stick" to the ground.
                m_VerticalSpeed = -gravity * k_StickingGravityProportion;

                if (m_Input.JumpInput && m_ReadyToJump)
                {
                    if(m_JumpLoadingTime == 0f) {
                        PrepareJump();
                    }
                    m_JumpLoadingTime += Time.fixedDeltaTime;
                    jumpCursor.transform.position += new Vector3(transform.forward.x, 0, transform.forward.z).normalized * Time.fixedDeltaTime * 25f;
                } else if(m_ReadyToJump && m_JumpLoadingTime > 0){
                    // m_VerticalSpeed = jumpSpeed * m_JumpLoadingTime * 1f;
                    // m_ForwardSpeed = jumpSpeed * m_JumpLoadingTime * 2f;
                    m_ReadyToJump = false;
                    m_IsGrounded = false;
                    Jump();
                }
            }
            else
            {
                // If Player is airborne, the jump button is not held and Player is currently moving upwards...
                if (!m_Input.JumpInput && m_VerticalSpeed > 0.0f)
                {
                    // ... decrease Player's vertical speed.
                    // This is what causes holding jump to jump higher that tapping jump.
                    m_VerticalSpeed -= k_JumpAbortSpeed * Time.deltaTime;
                }

                // If a jump is approximately peaking, make it absolute.
                if (Mathf.Approximately(m_VerticalSpeed, 0f))
                {
                    m_VerticalSpeed = 0f;
                }
                
                // If Player is airborne, apply gravity.
                m_VerticalSpeed -= gravity * Time.deltaTime;
            }
        }

        // Called each physics step to set the rotation Player is aiming to have.
        void SetTargetRotation()
        {
            // Create three variables, move input local to the player, flattened forward direction of the camera and a local target rotation.
            Vector2 moveInput = m_Input.MoveInput;
            Vector3 localMovementDirection = new Vector3(moveInput.x, 0f, moveInput.y).normalized;
            
            Vector3 forward = Quaternion.Euler(0f, cameraSettings.Current.m_XAxis.Value, 0f) * Vector3.forward;
            forward.y = 0f;
            forward.Normalize();

            Quaternion targetRotation;
            
            // If the local movement direction is the opposite of forward then the target rotation should be towards the camera.
            if (Mathf.Approximately(Vector3.Dot(localMovementDirection, Vector3.forward), -1.0f))
            {
                targetRotation = Quaternion.LookRotation(-forward);
            }
            else
            {
                // Otherwise the rotation should be the offset of the input from the camera's forward.
                Quaternion cameraToInputOffset = Quaternion.FromToRotation(Vector3.forward, localMovementDirection);
                targetRotation = Quaternion.LookRotation(cameraToInputOffset * forward);
            }

            // The desired forward direction of Player.
            Vector3 resultingForward = targetRotation * Vector3.forward;

            // Find the difference between the current rotation of the player and the desired rotation of the player in radians.
            float angleCurrent = Mathf.Atan2(transform.forward.x, transform.forward.z) * Mathf.Rad2Deg;
            float targetAngle = Mathf.Atan2(resultingForward.x, resultingForward.z) * Mathf.Rad2Deg;

            m_AngleDiff = Mathf.DeltaAngle(angleCurrent, targetAngle);
            m_TargetRotation = targetRotation;
        }

        // Called each physics step after SetTargetRotation if there is move input and Player is in the correct animator state according to IsOrientationUpdated.
        void UpdateOrientation()
        {
            Vector3 localInput = new Vector3(m_Input.MoveInput.x, 0f, m_Input.MoveInput.y);
            float groundedTurnSpeed = Mathf.Lerp(maxTurnSpeed, minTurnSpeed, m_ForwardSpeed / m_DesiredForwardSpeed);
            float actualTurnSpeed = m_IsGrounded ? groundedTurnSpeed : Vector3.Angle(transform.forward, localInput) * k_InverseOneEighty * k_AirborneTurnSpeedProportion * groundedTurnSpeed;
            m_TargetRotation = Quaternion.RotateTowards(transform.rotation, m_TargetRotation, actualTurnSpeed * Time.deltaTime);

            transform.rotation = m_TargetRotation;
        }

        // Called each physics step (so long as the Animator component is set to Animate Physics) after FixedUpdate to override root motion.
        void OnAnimatorMove()
        {
            Vector3 movement;

            // // If Player is on the ground...
            // if (m_IsGrounded)
            // {
            //     // ... raycast into the ground...
            //     RaycastHit hit;
            //     Ray ray = new Ray(transform.position + Vector3.up * k_GroundedRayDistance * 0.5f, -Vector3.up);
            //     if (Physics.Raycast(ray, out hit, k_GroundedRayDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            //     {
            //         // ... and get the movement of the root motion rotated to lie along the plane of the ground.
            //         movement = Vector3.ProjectOnPlane(m_Animator.deltaPosition, hit.normal);
                    
            //         // Also store the current walking surface so the correct audio is played.
            //         Renderer groundRenderer = hit.collider.GetComponentInChildren<Renderer>();
            //         m_CurrentWalkingSurface = groundRenderer ? groundRenderer.sharedMaterial : null;
            //     }
            //     else
            //     {
            //         // If no ground is hit just get the movement as the root motion.
            //         // Theoretically this should rarely happen as when grounded the ray should always hit.
            //         movement = m_Animator.deltaPosition;
            //         m_CurrentWalkingSurface = null;
            //     }
            // }
            // else
            // {
            //     // If not grounded the movement is just in the forward direction.
            //     movement = m_ForwardSpeed * transform.forward * Time.deltaTime;
            // }

            // // Rotate the transform of the character controller by the animation's root rotation.
            // m_CharCtrl.transform.rotation *= m_Animator.deltaRotation;

            movement = m_ForwardSpeed * transform.forward * Time.deltaTime;

            // Add to the movement with the calculated vertical speed.
            movement += m_VerticalSpeed * Vector3.up * Time.deltaTime;

            // Move the character controller.
            m_CharCtrl.Move(movement);

            // After the movement store whether or not the character controller is grounded.
            m_IsGrounded = m_CharCtrl.isGrounded;

            // If Player is not on the ground then send the vertical speed to the animator.
            // This is so the vertical speed is kept when landing so the correct landing animation is played.
            // if (!m_IsGrounded)
            //     m_Animator.SetFloat(m_HashAirborneVerticalSpeed, m_VerticalSpeed);

            // // Send whether or not Player is on the ground to the animator.
            m_Animator.SetBool("grounded", m_IsGrounded);
        }

        void PrepareJump(){
            // Particules?
            // Mark on the ground
            if(isOnOlympus){
                jumpCursor.transform.parent = null;
                jumpCursor.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
                jumpCursor.transform.position += new Vector3(transform.forward.x, 0, transform.forward.z).normalized * 15f;
                jumpCursor.SetActive(true);
            }
        }

        void Jump(){
            // Change Camera
            if (isOnOlympus){
                cameraSettings.StartJump();
                fallAttack.Jump();
                StartCoroutine(ExecuteJump());
            } else {
                cameraSettings.BackToOlympus(olympusRespawn, transform);
                StartCoroutine(ExecuteJump());
            }
        }

        IEnumerator ExecuteJump()
        {
            jumpCursor.SetActive(false);
            //yield return new WaitForSeconds(1.25f);
            m_VerticalSpeed = jumpSpeed * 4f;
            if(m_isOnOlympus){
                m_ForwardSpeed = jumpSpeed * 4f;
            }
            m_IsGrounded = false;
            m_JumpLoadingTime = 0f;
            
            if(m_isOnOlympus)
            {
                yield return new WaitForSeconds(0.3f);
                m_isOnOlympus = false;
                m_fallingDestination = jumpCursor.transform.position;
                cameraSettings.Fall(m_fallingDestination);
                StartCoroutine(ExecuteFallAttack());
            }
            else
            {
                //yield return new WaitForSeconds(0.5f);
                m_isOnOlympus = true;
                m_respawn = true;
                cameraSettings.Respawn();

                yield return new WaitForSeconds(0.5f);
                cameraSettings.EndRespawn();
            }
        }
        IEnumerator ExecuteFallAttack()
        {
            while(!m_IsGrounded){
                yield return null;
            }
            fallAttack.Attack();
        }

        /// <summary>
        /// OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("Soldier") || other.CompareTag("City")){
                ArmyAI soldier = other.GetComponent<ArmyAI>();
                if(!soldier.isPlayerArmy){
                    enemiesInRange.Add(soldier);
                }
            }
        }

        /// <summary>
        /// OnTriggerExit is called when the Collider other has stopped touching the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerExit(Collider other)
        {
            if(other.CompareTag("Soldier") || other.CompareTag("City")){
                ArmyAI soldier = other.GetComponent<ArmyAI>();
                if(!soldier.isPlayerArmy){
                    enemiesInRange.Remove(soldier);
                }
            }
        }
    }
}