﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alakajam {
    public class CityController : MonoBehaviour
    {
        public GameObject zeusCity;
        public GameObject playerCity;

        private float soldierGainRate;
        private float nextSoldierGain = 0f;

        ArmyAI armyAI;

        void Awake()
        {
            armyAI = GetComponent<ArmyAI>();
        }

        private void Start()
        {
            SelectRightCity();
            armyAI.OnCityCaptured += ChangeGod;
        }

        private void ChangeGod()
        {
            armyAI.isPlayerArmy = !armyAI.isPlayerArmy;
            SelectRightCity();
        }

        private void SelectRightCity()
        {
            soldierGainRate = GameManager.instance.soldierGainRate;
            nextSoldierGain = Time.time + (armyAI.isPlayerArmy ? soldierGainRate * 2f/3 : soldierGainRate);
            if (armyAI.isPlayerArmy)
            {
                zeusCity.gameObject.SetActive(false);
                playerCity.gameObject.SetActive(true);
            }
            else
            {
                zeusCity.gameObject.SetActive(true);
                playerCity.gameObject.SetActive(false);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if(!armyAI.IsUnderAttack() && nextSoldierGain < Time.time - soldierGainRate){
                nextSoldierGain = Time.time + soldierGainRate;
                armyAI.health.GainHealth(1);
            }
        }
    }
}