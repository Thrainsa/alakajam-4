﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alakajam {
    public class GameManager : MonoBehaviour
    {
        protected static GameManager s_Instance;
        public static GameManager instance { get { return s_Instance; } }

        public List<ArmyAI> armies = new List<ArmyAI>();
        public float soldierGainRate = 0.7f;
        public int jumpDamage = 5;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            s_Instance = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            ArmyAI[] armies = Resources.FindObjectsOfTypeAll<ArmyAI>();
            foreach (var item in armies)
            {
                this.armies.Add(item);
            }
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public int GetZeusFollowers(){
            return GetFollowers(false);
        }

        public int GetPlayerFollowers(){
            return GetFollowers(true);
        }

        private int GetFollowers(bool isPlayer){
            int res = 0;

            armies.ForEach(item => {
                if(isPlayer == item.isPlayerArmy){
                    res += item.soldierCount;
                }
            });

            return res;
        }
    }
}