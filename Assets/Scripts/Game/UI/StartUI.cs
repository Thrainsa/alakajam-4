﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Alakajam
{
    // Mostly from 3DGameKit
    public class StartUI : MonoBehaviour
    {
        public bool alwaysDisplayMouse;
        public GameObject pauseCanvas;
        public GameObject optionsCanvas;
        public GameObject controlsCanvas;
        public GameObject audioCanvas;
        public GameObject victoryCanvas;

        protected bool m_InPause;
        protected PlayableDirector[] m_Directors;

        private bool victoryShownOnce = false;
        private bool victoryShutOnce = false;

        void Start()
        {
            if (!alwaysDisplayMouse)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }

            m_Directors = FindObjectsOfType<PlayableDirector> ();
        }

        public void Quit()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
		    Application.Quit();
#endif
        }

        public void ExitPause()
        {
            m_InPause = true;
            if (victoryShownOnce) victoryShutOnce = true;
            SwitchPauseState();
        }

        public void RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            m_InPause = false;
            Time.timeScale = 1;
            //m_InPause = true;
            //SwitchPauseState();
            // SceneController.RestartZone();
        }

        void Update()
        {
            if (PlayerInput.Instance != null && PlayerInput.Instance.Pause)
            {
                SwitchPauseState();
            }

            if ((Time.frameCount % 50) == 0 && !victoryShownOnce)
            {
                ArmyAI[] ais = FindObjectsOfType<ArmyAI>();
                bool enemyFound = false;
                foreach (ArmyAI ai in ais)
                {
                    if (!ai.isPlayerArmy)
                    {
                        enemyFound = true;
                        break;
                    }
                }
                if (!enemyFound)
                {
                    victoryCanvas.SetActive(true);
                    victoryShownOnce = true;
                }
            }
        }

        protected void SwitchPauseState()
        {
            if (m_InPause && Time.timeScale > 0)
                return;

            if (!alwaysDisplayMouse)
            {
                Cursor.lockState = m_InPause ? CursorLockMode.Locked : CursorLockMode.None;
                Cursor.visible = !m_InPause;
            }

            for (int i = 0; i < m_Directors.Length; i++)
            {
                if (m_Directors[i].state == PlayState.Playing && !m_InPause)
                {
                    m_Directors[i].Pause ();
                }
                else if(m_Directors[i].state == PlayState.Paused && m_InPause)
                {
                    m_Directors[i].Resume ();
                }
            }

            if (m_InPause)
                PlayerInput.Instance.GainControl();
            else
                PlayerInput.Instance.ReleaseControl();

            Time.timeScale = m_InPause ? 1 : 0;

            if (victoryShownOnce && victoryShutOnce)
            {
                victoryCanvas.SetActive(!m_InPause);
            }
            else
            {
                if (pauseCanvas)
                    pauseCanvas.SetActive(!m_InPause);
            }

            if (optionsCanvas)
                optionsCanvas.SetActive(false);

            if (controlsCanvas)
                controlsCanvas.SetActive(false);

            if (audioCanvas)
                audioCanvas.SetActive(false);

            m_InPause = !m_InPause;
        }
    }
}
