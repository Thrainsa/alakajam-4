﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.AI;

namespace Alakajam
{
    // Mostly from 3DGameKit
    public class CameraSettings : MonoBehaviour
    {
        private const float defaultBlendTime = 0.3f;
        private Vector3 initialCameraOffset = new Vector3(0, 4, 7);

        public enum InputChoice
        {
            KeyboardAndMouse, Controller,
        }

        [Serializable]
        public struct InvertSettings
        {
            public bool invertX;
            public bool invertY;
        }

        public Transform follow;
        public Transform lookAt;
        public Transform olympusRespawn;
        public CinemachineBrain cinemachineBrain;
        public CinemachineFreeLook keyboardAndMouseCamera;
        public CinemachineFreeLook controllerCamera;
        public CinemachineFreeLook olympusKeyboardAndMouseCamera;
        public CinemachineFreeLook olympusControllerCamera;
        public CinemachineVirtualCamera jumpCamera;
        public CinemachineVirtualCamera fallingCamera;

        public InputChoice inputChoice;
        public InvertSettings keyboardAndMouseInvertSettings;
        public InvertSettings controllerInvertSettings;
        public bool allowRuntimeCameraSettingsChanges;

        public CinemachineFreeLook Current
        {
            get {
                if(playerController.isOnOlympus){
                    return inputChoice == InputChoice.KeyboardAndMouse ? olympusKeyboardAndMouseCamera : olympusControllerCamera;
                } else {
                    return inputChoice == InputChoice.KeyboardAndMouse ? keyboardAndMouseCamera : controllerCamera;
                }
            }
        }

        private PlayerController playerController;

        void Reset()
        {
            playerController = FindObjectOfType<PlayerController>();
            if (playerController != null)
            {
                follow = playerController.transform;
                lookAt = follow.Find("HeadTarget");

                if (playerController.cameraSettings == null)
                    playerController.cameraSettings = this;
            }

            Transform keyboardAndMouseCameraTransform = transform.Find("KeyboardAndMouseFreeLookRig");
            if (keyboardAndMouseCameraTransform != null)
                keyboardAndMouseCamera = keyboardAndMouseCameraTransform.GetComponent<CinemachineFreeLook>();

            Transform controllerCameraTransform = transform.Find("ControllerFreeLookRig");
            if (controllerCameraTransform != null)
                controllerCamera = controllerCameraTransform.GetComponent<CinemachineFreeLook>();

            Transform olympusKeyboardAndMouseCameraTransform = transform.Find("OlympusKeyboardAndMouseFreeLookRig");
            if (olympusKeyboardAndMouseCameraTransform != null)
                olympusKeyboardAndMouseCamera = olympusKeyboardAndMouseCameraTransform.GetComponent<CinemachineFreeLook>();

            Transform olympusControllerCameraTransform = transform.Find("OlympusControllerFreeLookRig");
            if (olympusControllerCameraTransform != null)
                olympusControllerCamera = olympusControllerCameraTransform.GetComponent<CinemachineFreeLook>();
            
            Transform jumpCameraTransform = transform.Find("JumpCameraVirtualRig");
            if (jumpCameraTransform != null)
                jumpCamera = jumpCameraTransform.GetComponent<CinemachineVirtualCamera>();

            Transform fallingCameraTransform = transform.Find("FallingCameraVirtualRig");
            if (fallingCameraTransform != null)
                fallingCamera = fallingCameraTransform.GetComponent<CinemachineVirtualCamera>();

        }

        private void InitCamera(CinemachineFreeLook camera, InvertSettings invertSettings)
        {
            if(camera != null){
                camera.Follow = follow;
                camera.LookAt = lookAt;
                camera.m_XAxis.m_InvertInput = invertSettings.invertX;
                camera.m_YAxis.m_InvertInput = invertSettings.invertY;
                camera.transform.position = lookAt.position - new Vector3(0, 3, 7);
            }
        }

        void Awake()
        {
            playerController = FindObjectOfType<PlayerController>();
            UpdateCameraSettings();
            olympusRespawn.transform.parent = null;
        }

        void Update()
        {
            if (allowRuntimeCameraSettingsChanges)
            {
                UpdateCameraSettings();
            }
        }

        void UpdateCameraSettings()
        {
            InitCamera(keyboardAndMouseCamera, keyboardAndMouseInvertSettings);
            InitCamera(controllerCamera, controllerInvertSettings);
            InitCamera(olympusKeyboardAndMouseCamera, keyboardAndMouseInvertSettings);
            InitCamera(olympusControllerCamera, controllerInvertSettings);

            keyboardAndMouseCamera.Priority = inputChoice == InputChoice.KeyboardAndMouse && !playerController.isOnOlympus ? 1 : 0;
            controllerCamera.Priority = inputChoice == InputChoice.Controller && !playerController.isOnOlympus ? 1 : 0;
            olympusKeyboardAndMouseCamera.Priority = inputChoice == InputChoice.KeyboardAndMouse && playerController.isOnOlympus ? 1 : 0;
            olympusControllerCamera.Priority = inputChoice == InputChoice.Controller && playerController.isOnOlympus ? 1 : 0;
        }

        public void StartJump() {
            cinemachineBrain.m_DefaultBlend.m_Time = 1f;
            jumpCamera.LookAt = lookAt;
            jumpCamera.Priority = 2;
            PlayerController.instance.GetComponent<NavMeshAgent>().enabled = false;
        }

        public void Fall(Vector3 destination) {
            cinemachineBrain.m_DefaultBlend.m_Time = defaultBlendTime;
            fallingCamera.transform.position = playerController.transform.position - 5*playerController.transform.forward + new Vector3(0,0,8);
            fallingCamera.LookAt = lookAt;
            fallingCamera.Priority = 2;
            jumpCamera.Priority = 0;
        }

        public void EndJump() {
            cinemachineBrain.m_DefaultBlend.m_Time = 1f;
            fallingCamera.Priority = 0;
            cinemachineBrain.m_DefaultBlend.m_Time = defaultBlendTime;
            PlayerController.instance.GetComponent<NavMeshAgent>().enabled = true;
        }

        public void BackToOlympus(Transform olympusRespawn, Transform playerTransform)
        {
            PlayerController.instance.GetComponent<NavMeshAgent>().enabled = false;
            fallingCamera.transform.position = olympusRespawn.position - 5*playerTransform.transform.forward;
            fallingCamera.LookAt = lookAt;
            cinemachineBrain.m_DefaultBlend.m_Time = 0f;
            fallingCamera.Priority = 2;
            jumpCamera.Priority = 0;
        }

        public void Respawn()
        {
            jumpCamera.transform.position = olympusRespawn.position + initialCameraOffset;
            jumpCamera.LookAt = lookAt;
            cinemachineBrain.m_DefaultBlend.m_Time = 0f;
            jumpCamera.Priority = 2;
            fallingCamera.Priority = 0;
        }

        public void EndRespawn()
        {
            jumpCamera.Priority = 0;
            cinemachineBrain.m_DefaultBlend.m_Time = defaultBlendTime;
            PlayerController.instance.GetComponent<NavMeshAgent>().enabled = true;
        }
    } 
}
