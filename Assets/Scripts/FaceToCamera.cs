﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alakajam
{
    public class FaceToCamera : MonoBehaviour
    {

        public Camera cameraToFace;

        // Start is called before the first frame update
        void Start()
        {
            cameraToFace = PlayerController.instance.cameraSettings.GetComponentInChildren<Camera>();
        }

        // Update is called once per frame
        void Update()
        {
            // Vector3 v = cameraToFace.transform.position - transform.position;
            // v.x = v.z = 0.0f;
            // transform.LookAt(cameraToFace.transform.position - new Vector3(0, cameraToFace.transform.position.y - transform.position.y, 0));

            transform.rotation = Quaternion.LookRotation(transform.position - cameraToFace.transform.position);
        }
    }
}