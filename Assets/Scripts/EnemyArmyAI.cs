﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alakajam
{
    public class EnemyArmyAI : MonoBehaviour
    {
        private static float lastArmyAttacked = 0;

        private ArmyAI[] allArmyAi;
        private ArmyAI myArmyAI;

        private float nextCheck;

        private bool debug = false;

        private void Start()
        {
            lastArmyAttacked = 0;
            nextCheck = Time.time + (debug ? 1 : Random.Range(45, 75) );
            allArmyAi = FindObjectsOfType<ArmyAI>();
            myArmyAI = GetComponent<ArmyAI>();
        }

        private void Update()
        {
            if (Time.time > nextCheck)
            {
                nextCheck = Time.time + (debug ? 1f : Random.Range(30, 50));
                
                if (!myArmyAI.isPlayerArmy)
                {
                    if (Time.time - lastArmyAttacked < 5)
                        return;

                    int score = GetZeusScore(); // Beginning: 320
                    int watchCities = (int) Mathf.Ceil((320f - score) / 70f);
                    if (score < (debug ? 900 : 300))
                    {
                        List<ArmyAI> aresAis = GetNearestCities(watchCities);
                        foreach (ArmyAI aresAi in aresAis)
                        {
                            if (aresAi.isPlayerArmy)
                            {
                                if ((aresAi.health.currentHealth / 2.3f) < myArmyAI.health.currentHealth
                                        || Random.Range(0, 1) < 0.15f)
                                {
                                    myArmyAI.SetDestination(aresAi.transform);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private int GetZeusScore()
        {
            int score = 0;
            foreach (ArmyAI ai in allArmyAi)
            {
                if (!ai.isPlayerArmy) score += ai.health.currentHealth;
            }
            return score;
        }

        private List<ArmyAI> GetNearestCities(int count)
        {
            List<ArmyAI> currentList = new List<ArmyAI>();
            return GetNearestCities(currentList, count);
        }

        private List<ArmyAI> GetNearestCities(List<ArmyAI> currentList, int count)
        {
            float distance = -1;
            ArmyAI winner = null;
            foreach (ArmyAI ai in allArmyAi)
            {
                if (ai != myArmyAI && !currentList.Contains(ai))
                {
                    float aiDistance = Vector3.Distance(myArmyAI.transform.position, ai.transform.position);
                    if (distance == -1 ||aiDistance < distance)
                    {
                        distance = aiDistance;
                        winner = ai;
                    }
                }
            }
            currentList.Add(winner);
            if (count > 1)
            {
                return GetNearestCities(currentList, count - 1);   
            } else
            {
                return currentList;
            }
        }

    }
}
