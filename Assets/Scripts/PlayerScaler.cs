﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alakajam {
    public class PlayerScaler : MonoBehaviour
    {
        public bool isPlayer;
        public Vector3Variable scale;

        float fullScalePosY = 23f;

        // Start is called before the first frame update
        void Start()
        {
            if(isPlayer){
                scale.SetValue(transform.localScale);
            }
        }

        // Update is called once per frame
        void Update()
        {
            // Disabled for now
            return;

            if(PlayerController.instance.isOnOlympus){
                scale.SetValue(Vector3.one);
            }
            else
            {
                scale.SetValue(Vector3.one * 0.1f);
            }
            transform.localScale = scale.Value;
        }
    }
}