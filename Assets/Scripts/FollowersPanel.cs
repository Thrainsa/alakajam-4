﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Alakajam {
    public class FollowersPanel : MonoBehaviour
    {
        public TextMeshPro zeusText;
        public TextMeshPro playerText;

        // Start is called before the first frame update
        void Start()
        {
            
        }

        /// <summary>
        /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
        /// </summary>
        void FixedUpdate()
        {
            zeusText.SetText("Zeus: "+GameManager.instance.GetZeusFollowers().ToString());
            playerText.SetText("Player: "+GameManager.instance.GetPlayerFollowers().ToString());
        }
    }
}