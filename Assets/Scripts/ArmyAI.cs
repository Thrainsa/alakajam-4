﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

namespace Alakajam {
    public class ArmyAI : MonoBehaviour
    {
        private const float battleIntervalMs = 0.5f;
        private const float walkSpeed = 3.5f;

        NavMeshAgent agent;

        public delegate void CityCaptured();
        public event CityCaptured OnCityCaptured;

        public ArmyAI zeusSoldier;
        public ArmyAI playerSoldier;
        
        public bool isPlayerArmy;
        public Transform destination;
        public GameObject hover;

        internal void TakeDamage(object jumpDamage)
        {
            throw new NotImplementedException();
        }

        public TextMeshPro text;
        [HideInInspector]
        public Damageable health;
        public int soldierCount { get { return health != null ? health.currentHealth : 0 ; } }
        protected float lastBattleTime = 0;
        private bool destinationJustChanged = false;

        void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
            health = GetComponent<Damageable>();
            health.Init(health.maxHealth);
            health.OnDeath += OnDeath;
        }

        // Start is called before the first frame update
        void Start()
        {
            SetDestination(this.destination);
        }

        // Update is called once per frame
        void Update()
        {
            var healthText = health.currentHealth.ToString();
            if(gameObject.CompareTag("City")){
                healthText += "/" + health.maxHealth; 
            }
            text.SetText(healthText);
        }

        void FixedUpdate()
        {
            // Destination reached!
            if (destination && !destinationJustChanged && agent.remainingDistance < 3)
            {
                // Was it a marker? (In that case let's destroy it)
                bool wasAMarker = DestroyTargetMarker();
                if (!wasAMarker)
                {
                    // No! Should be an AI then
                    ArmyAI armyAI = destination.GetComponent<ArmyAI>();
                    if (armyAI != null)
                    {
                        // Should I fight it?
                        if (armyAI.isPlayerArmy != isPlayerArmy)
                        {
                            bool battleOver = BattleWith(armyAI);
                            if (battleOver && armyAI.CompareTag("City"))
                            {
                                SetDestination(null);
                                // enter city
                                CombineWithArmy(armyAI);
                            }
                        }
                        else
                        {
                            // Combine armies or enter city
                            CombineWithArmy(armyAI);
                        }

                    } else
                    {
                        Debug.LogError("Why was my destination a...");
                        Debug.LogError(armyAI);
                        destination = null;
                    }
                }
            }

            destinationJustChanged = false;
        }

        private void CombineWithArmy(ArmyAI armyAI)
        {
            int healthLost = armyAI.health.GainHealth(this.health.currentHealth);
            health.TakeDamage(healthLost);
            if (health.currentHealth > 0)
            {
                SetDestination(null);
            }
            else
            {
                OnDeath();
            }
        }

        void OnDeath(){
            if(gameObject != null){
                if(gameObject.CompareTag("City")){
                    OnCityCaptured();
                } else {
                    GameManager.instance.armies.Remove(this);
                    Destroy(gameObject);
                    DestroyTargetMarker();
                }
            }
        }

        public bool DestroyTargetMarker()
        {
            bool wasAMarker = false;
            if (destination != null && destination.CompareTag("TargetMarker"))
            {
                wasAMarker = true;
                Destroy(destination.gameObject);
                destination = null;
            }
            return wasAMarker;
        }

        public void SetHoverVisible(bool visible)
        {
            if (hover != null)
            {
                hover.SetActive(visible);
            }
        }

        public bool SetDestination(Transform destination)
        {
            if(destination != null) {
                if(gameObject.CompareTag("City"))
                {
                    var value = soldierCount -1;
                    if (value <= 0) {
                        return false;
                    }
                    ArmyAI armyPrefab = isPlayerArmy ? playerSoldier : zeusSoldier;
                    ArmyAI army = Instantiate(armyPrefab, transform.position, Quaternion.Euler(destination.position - transform.position));
                    GameManager.instance.armies.Add(army);
                    health.TakeDamage(value);
                    army.health.Init(value);
                    army.SetDestination(destination);
                } else {
                    agent.destination = destination.position;
                    agent.speed = walkSpeed * (isPlayerArmy ? 1 : 0.6f);
                }
            }
            if (agent != null) {
                this.destination = destination;
                destinationJustChanged = true;
            }
            return true;
        }
        
        /**
         * returns true if the battle is over
         */
        public bool BattleWith(ArmyAI otherArmy)
        {
            if (lastBattleTime + battleIntervalMs < Time.fixedTime)
            {
                this.TakeDamage(1);
                otherArmy.TakeDamage(1);
            }

            return otherArmy.health.currentHealth <= 0 || this.health.currentHealth <= 0;
        }

        public bool TakeDamage(int amount)
        {
            this.lastBattleTime = Time.fixedTime;
            return this.health.TakeDamage(amount);
        }

        public bool IsUnderAttack(){
            return Time.fixedTime < lastBattleTime + 2f;
        }

    }
}