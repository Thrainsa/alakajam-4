﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Alakajam {
    public class FallAttack : MonoBehaviour
    {
        List<ArmyAI> enemies = new List<ArmyAI>();
        Collider colliderTrigger;
        
        public GameObject particuleSystemPrefab;

        void Awake()
        {
            colliderTrigger = GetComponent<Collider>();
            colliderTrigger.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void Jump(){
            colliderTrigger.gameObject.SetActive(true);
            enemies.Clear();
        }

        public void Attack(){
            enemies.ForEach(army => army.TakeDamage(GameManager.instance.jumpDamage));
            StartCoroutine(ShowParticules());
        }

        IEnumerator ShowParticules()
        {
            GameObject particules = Instantiate(particuleSystemPrefab,transform.position,Quaternion.identity);
            yield return new WaitForSeconds(2f);
            Destroy(particules);
            colliderTrigger.gameObject.SetActive(false);
            enemies.Clear();
        }

        /// <summary>
        /// OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerEnter(Collider other)
        {
            ArmyAI army = other.GetComponent<ArmyAI>();
            if(army != null && !army.isPlayerArmy){
                enemies.Add(army);
            }
        }
    }
}